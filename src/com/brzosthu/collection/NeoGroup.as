package com.brzosthu.collection
{
    import flash.events.Event;
    import flash.events.EventDispatcher;

    import mx.collections.ArrayCollection;
    import mx.events.CollectionEvent;

    public class NeoGroup extends EventDispatcher
    {
        [Bindable]
        public var GroupLabel:String;

        [Bindable]
        public var hasChildren:Boolean;

        public function NeoGroup()
        {
            super();
            children = new ArrayCollection();
        }

        private var _children:ArrayCollection;

        [Bindable(event="childrenChanged")]
        public function get children():ArrayCollection
        {
            return _children;
        }

        public function set children(value:ArrayCollection):void
        {
            if (_children == value) return;

            if(_children){
                _children.removeEventListener(CollectionEvent.COLLECTION_CHANGE,
                        children_collectionChangeHandler);
            }

            _children = value;

            if(_children){
                _children.addEventListener(CollectionEvent.COLLECTION_CHANGE,
                        children_collectionChangeHandler);
            }

            children_collectionChangeHandler();
            dispatchEvent(new Event("childrenChanged"));
        }

        private function children_collectionChangeHandler(event:CollectionEvent = null):void
        {
            hasChildren = _children && _children.length > 0;
        }
    }
}
