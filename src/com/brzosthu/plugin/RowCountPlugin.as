package com.brzosthu.plugin
{
    import flash.events.Event;
    import flash.events.EventDispatcher;

    import mx.core.UIComponent;

    [Event(type="flash.events.Event", name="rowCountChanged")]
    public class RowCountPlugin extends EventDispatcher
    {
        public function RowCountPlugin()
        {
            super();
        }

        private var _dataGridContainer:UIComponent;

        [Bindable(event="dataGridContainerChanged")]
        public function get dataGridContainer():UIComponent
        {
            return _dataGridContainer;
        }

        public function set dataGridContainer(value:UIComponent):void
        {
            if (_dataGridContainer == value) return;

            if (_dataGridContainer)
            {
                _dataGridContainer.removeEventListener(Event.RESIZE,
                        dataGridContainer_resizeHandler);
            }

            _dataGridContainer = value;

            if (_dataGridContainer)
            {
                _dataGridContainer.addEventListener(Event.RESIZE,
                        dataGridContainer_resizeHandler);
            }

            calculateRowCount();
            dispatchEvent(new Event("dataGridContainerChanged"));
        }

        private var _rowHeight:int = 25;

        [Bindable(event="rowHeightChanged")]
        public function get rowHeight():int
        {
            return _rowHeight;
        }

        public function set rowHeight(value:int):void
        {
            if (_rowHeight == value) return;
            _rowHeight = value;
            calculateRowCount();
            dispatchEvent(new Event("rowHeightChanged"));
        }

        private var _headerHeight:int = 25;

        [Bindable(event="headerHeightChanged")]
        public function get headerHeight():int
        {
            return _headerHeight;
        }

        public function set headerHeight(value:int):void
        {
            if (_headerHeight == value) return;
            _headerHeight = value;
            calculateRowCount();
            dispatchEvent(new Event("headerHeightChanged"));
        }

        private var _rowCount:int = 20;

        [Bindable(event="rowCountChanged")]
        public function get rowCount():int
        {
            return _rowCount;
        }

        private function calculateRowCount():void
        {
            if (!dataGridContainer) return;

            var height:Number = dataGridContainer.getLayoutBoundsHeight();
            if (isNaN(height) || height <= 0)
            {
                return;
            }
            var newRowCount:int = (height - headerHeight) / rowHeight;

            if (newRowCount != _rowCount)
            {
                _rowCount = newRowCount;
                dispatchEvent(new Event("rowCountChanged"));
            }
        }

        private function dataGridContainer_resizeHandler(event:Event):void
        {
            calculateRowCount();
        }
    }
}
