package com.brzosthu.collection
{
    import mx.collections.HierarchicalCollectionView;
    import mx.collections.HierarchicalCollectionViewCursor;
    import mx.collections.ICollectionView;
    import mx.collections.IHierarchicalData;

    public class NeoCollectionViewCursor extends HierarchicalCollectionViewCursor
    {
        public function NeoCollectionViewCursor(collection:HierarchicalCollectionView, model:ICollectionView, hierarchicalData:IHierarchicalData)
        {
            super(collection, model, hierarchicalData);
        }
    }
}
