package com.brzosthu
{
    import com.brzosthu.collection.SnakeCollectionView;
    import com.frishy.collections.FastGroupingCollection;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import mx.collections.ArrayCollection;
    import mx.collections.ArrayList;
    import mx.collections.Grouping;
    import mx.collections.GroupingField;
    import mx.utils.ObjectProxy;

    public class SnakeViewAppPM extends EventDispatcher
    {
        [Bindable]
        public var collectionView:SnakeCollectionView;

        [Bindable]
        public var groupingCollection:FastGroupingCollection;

        [Bindable]
        public var collection:ArrayCollection;

        [Bindable]
        public var grouping:Grouping;

        [Bindable]
        public var list:ArrayList;

        [Bindable]
        public var filter:String;

        [Bindable]
        public var itemsPerSector:int = 6;

        [Bindable]
        public var totalItems:int = 12;

        [Bindable]
        public var rowCount:int = 10;

        [Bindable]
        public var collectionRowCount:int = 10;

        private var updateTimer:Timer;

        public function SnakeViewAppPM()
        {
            generateCollection();
        }

        private var _selectedViewMode:int = 0;

        [Bindable(event="selectedViewModeChanged")]
        public function get selectedViewMode():int
        {
            return _selectedViewMode;
        }

        public function set selectedViewMode(value:int):void
        {
            if (_selectedViewMode == value) return;
            _selectedViewMode = value;
            dispatchEvent(new Event("selectedViewModeChanged"));
        }

        public function updateCollection():void
        {
            generateInitialData();
            startUpdates();
        }

        public function updateRowCount():void
        {
            collectionRowCount = rowCount;
        }

        public function addMore():void
        {
            for (var i:int = 0; i < 20; i++)
            {
                list.addItem(getRow(list.length));
            }
        }

        public function filterFunction(item:Object):Boolean
        {
            if (!filter) return true;

            var children:ArrayCollection = item.children as ArrayCollection;
            if (children)
            {
                var childrenList:Array = children.source;
                var result:Array = childrenList.filter(childrenFilterFunction);
                if (result.length == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public function childrenFilterFunction(item:Object, index:int = -1, list:Array = null):Boolean
        {
            if (!filter) return true;

            var object:ObjectProxy = item as ObjectProxy;
            if (!object)
            {
                return true;
            }

            var sector:String = object.sector;
            return sector.toLowerCase().indexOf(filter) != -1;
        }

        private function generateCollection():void
        {
            list = new ArrayList();

            collection = new ArrayCollection();
            collection.list = list;

            var groupingField:GroupingField = new GroupingField("sector", true);

            grouping = new Grouping();
            grouping.fields = [groupingField];

            groupingCollection = new FastGroupingCollection();
            groupingCollection.grouping = grouping;
            groupingCollection.source = collection;

            collectionView = new SnakeCollectionView();
            collectionView.source = groupingCollection;

            collectionView.filterFunction = filterFunction;
            collectionView.refresh();
        }

        private function generateInitialData():void
        {
            collectionView.disableAutoUpdate();
            list.source = null;

            var data:Array = [];
            for (var i:int = 0; i < totalItems; i++)
            {
                data.push(getRow(i));
            }
            list.source = data;
            groupingCollection.refresh();
            collectionView.refresh();
            collectionView.enableAutoUpdate();
        }

        private function startUpdates():void
        {
            if (updateTimer)
            {
                updateTimer.stop()
            }

            updateTimer = new Timer(1000);
            updateTimer.addEventListener(TimerEvent.TIMER,
                    updateTimer_timerHandler);

            updateTimer.start();
        }

        private function updateData():void
        {
            var index:int = Math.random() * list.length;
            var row:ObjectProxy = list.getItemAt(index) as ObjectProxy;

            row.value = getRowValue();
        }

        private function getRow(index:int):ObjectProxy
        {
            var row:ObjectProxy = new ObjectProxy();
            var sector:int = index / itemsPerSector;

            row.sector = 'Sector ' + sector;
            row.index = index;
            row.value = getRowValue();

            return row;
        }

        private function getRowValue():int
        {
            return Math.floor(Math.random() * 1000);
        }

        private function updateTimer_timerHandler(event:TimerEvent):void
        {
            updateData();
        }
    }
}
