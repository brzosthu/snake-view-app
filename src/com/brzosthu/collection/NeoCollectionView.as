package com.brzosthu.collection
{
    import mx.collections.HierarchicalCollectionView;
    import mx.collections.ICollectionView;
    import mx.collections.IHierarchicalData;
    import mx.collections.IViewCursor;
    import mx.collections.errors.ItemPendingError;
    import mx.core.mx_internal;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;

    use namespace mx_internal;

    public class NeoCollectionView extends HierarchicalCollectionView
    {
        public function NeoCollectionView(hierarchicalData:IHierarchicalData = null,
                                          argOpenNodes:Object = null)
        {
            super(hierarchicalData, argOpenNodes);
        }

        override public function createCursor():IViewCursor
        {
            return new NeoCollectionViewCursor(
                    this, treeData, source);
        }

        override public function nestedCollectionChangeHandler(event:CollectionEvent):void
        {
            super.nestedCollectionChangeHandler(event);

            var i:int;
            var n:int;
            var parentOfChangingNode:Object;
            var changingNode:Object;
            var convertedEvent:CollectionEvent;
            var ce:CollectionEvent = event as CollectionEvent;

            if (!ce)
            {
                return;
            }

            if (event.kind == CollectionEventKind.MOVE)
            {

                n = event.items.length;
                convertedEvent = new CollectionEvent(
                        CollectionEvent.COLLECTION_CHANGE,
                        false,
                        true,
                        event.kind);

                for (i = 0; i < n; i++)
                {
                    changingNode = ce.items[i];
                    parentOfChangingNode = getParentItem(changingNode);
                    if(parentOfChangingNode){
                        break;
                    }
                }

                convertedEvent.oldLocation = getVisibleLocationInSubCollection(parentOfChangingNode, ce.oldLocation);
                convertedEvent.location = getVisibleLocationInSubCollection(parentOfChangingNode, ce.location);
                dispatchEvent(convertedEvent);
            }
        }

        private function getVisibleLocationInSubCollection(parent:Object, oldLocation:int):int
        {
            var newLocation:int = oldLocation;
            var target:Object = parent;
            parent = getParentItem(parent);
            var children:ICollectionView;
            var cursor:IViewCursor;
            while (parent != null)
            {
                children = getChildren(parent);
                cursor = children.createCursor();
                while (!cursor.afterLast)
                {
                    if (cursor.current == target)
                    {
                        newLocation++;
                        break;
                    }
                    newLocation += calculateLength(cursor.current, parent) + 1;
                    try
                    {
                        cursor.moveNext();
                    }
                    catch (e:ItemPendingError)
                    {
                        break;
                    }
                }
                target = parent;
                parent = getParentItem(parent);
            }
            cursor = treeData.createCursor();
            while (!cursor.afterLast)
            {
                if (cursor.current == target)
                {
                    newLocation++;
                    break;
                }
                newLocation += calculateLength(cursor.current, parent) + 1;
                try
                {
                    cursor.moveNext();
                }
                catch (e:ItemPendingError)
                {
                    break;
                }
            }
            return newLocation;
        }

    }
}
