package com.brzosthu.collection
{
    import mx.collections.ArrayCollection;
    import mx.collections.CursorBookmark;
    import mx.collections.HierarchicalData;
    import mx.collections.IHierarchicalCollectionView;
    import mx.collections.IViewCursor;
    import mx.utils.ObjectProxy;

    import org.flexunit.asserts.assertEquals;
    import org.flexunit.asserts.assertFalse;
    import org.flexunit.asserts.assertNotNull;
    import org.flexunit.asserts.assertTrue;

    public class SnakeChildCollectionViewCursorTest
    {
        private var sourceCollection:SnakeCollectionView;

        private var childCollection:SnakeChildCollectionView;

        [Before]
        public function setUp():void
        {
            sourceCollection = new SnakeCollectionView(new HierarchicalData());

            for (var i:int = 0; i < 2; i++)
            {
                var group:ObjectProxy = new ObjectProxy();
                group.index = i;
                group.children = new ArrayCollection();

                sourceCollection.addChild(null, group);

                for (var j:int = 0; j < 6; j++)
                {
                    var item:ObjectProxy = new ObjectProxy();
                    item.index = i * 4 + j;

                    sourceCollection.addChild(group, item);
                }
            }

            expandAll(sourceCollection);

            childCollection = new SnakeChildCollectionView();
            childCollection.offset = 10;
            childCollection.rowCount = 10;
            childCollection.sourceCollection = sourceCollection;
        }

        [Test]
        public function testNeoCollectionViewInitialState():void
        {
            var cursor:IViewCursor = sourceCollection.createCursor();
            assertNotNull(cursor.current);
            assertFalse(cursor.afterLast);
            assertFalse(cursor.beforeFirst);

            cursor.seek(CursorBookmark.FIRST);
            assertFalse(cursor.beforeFirst);
            assertFalse(cursor.movePrevious());

            cursor.seek(CursorBookmark.LAST);
            assertFalse(cursor.afterLast);
            assertFalse(cursor.moveNext());
        }

        [Test]
        public function testChildCollectionInitialState():void
        {
            var cursor:IViewCursor = childCollection.createCursor();
            assertNotNull(cursor.current);
            assertFalse(cursor.afterLast);
            assertFalse(cursor.beforeFirst);

            cursor.seek(CursorBookmark.FIRST);
            assertFalse(cursor.beforeFirst);
            assertFalse(cursor.movePrevious());

            cursor.seek(CursorBookmark.LAST);
            assertFalse(cursor.afterLast);
            assertFalse(cursor.moveNext());
        }

        [Test]
        public function testOpenCloseNode():void
        {
            var cursor:SnakeChildCollectionViewCursor = childCollection.createCursor() as SnakeChildCollectionViewCursor;

            var firstGroup:Object = sourceCollection.createCursor().current;

            assertNotNull(firstGroup);
            assertNotNull(cursor.current);

            sourceCollection.closeNode(firstGroup);

            cursor.seek(CursorBookmark.FIRST);

            assertEquals(0, cursor.index);

            assertTrue(cursor.beforeFirst);
            assertTrue(cursor.afterLast);

            sourceCollection.openNode(firstGroup);

            assertTrue(cursor.moveNext());

            cursor.seek(CursorBookmark.FIRST);

            assertEquals(0, cursor.index);
            assertFalse(cursor.beforeFirst);
            assertFalse(cursor.afterLast);
        }

        [After]
        public function tearDown():void
        {
            childCollection = null;
            sourceCollection = null;
        }

        private static function expandAll(collectionView:IHierarchicalCollectionView):void
        {
            var iterator:IViewCursor = collectionView.createCursor();
            iterator.seek(CursorBookmark.FIRST);

            while (!iterator.afterLast)
            {
                var item:Object = iterator.current;

                if (collectionView.getChildren(item))
                {
                    collectionView.openNode(item);
                }
                iterator.moveNext();
            }
        }
    }
}
