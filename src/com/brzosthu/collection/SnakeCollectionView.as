package com.brzosthu.collection
{
    import mx.collections.ICollectionView;
    import mx.collections.IHierarchicalData;
    import mx.collections.IViewCursor;
    import mx.collections.errors.ItemPendingError;
    import mx.core.mx_internal;
    import mx.events.CollectionEvent;
    import mx.utils.UIDUtil;

    use namespace mx_internal;

    public class SnakeCollectionView extends NeoCollectionView
    {
        public function SnakeCollectionView(hierarchicalData:IHierarchicalData = null, argOpenNodes:Object = null)
        {
            super(hierarchicalData, argOpenNodes);
        }

        override public function openNode(node:Object):void
        {
            var uid:String = UIDUtil.getUID(node);

            if (openNodes[uid])
                return;

            var childCollection:ICollectionView = getChildren(node);
            if(!childCollection)
                return;

            super.openNode(node);

            var event:CollectionEvent = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
            event.kind = SnakeCollectionEventKind.NODE_OPEN;
            event.location = getVisibleLocationInSubCollection(node, -1);

            trace("node open", event.location);

            dispatchEvent(event);
        }

        override public function closeNode(node:Object):void
        {
            var uid:String = UIDUtil.getUID(node);

            if (!openNodes[uid])
                return;

            var childCollection:ICollectionView = getChildren(node);
            if(!childCollection)
                return;

            super.closeNode(node);

            var event:CollectionEvent = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
            event.kind = SnakeCollectionEventKind.NODE_CLOSE;
            event.location = getVisibleLocationInSubCollection(node, -1);

            trace("node close", event.location);

            dispatchEvent(event);
        }

        private function getVisibleLocationInSubCollection(parent:Object, oldLocation:int):int
        {
            var newLocation:int = oldLocation;
            var target:Object = parent;
            parent = getParentItem(parent);
            var children:ICollectionView;
            var cursor:IViewCursor;
            while (parent != null)
            {
                children = getChildren(parent);
                cursor = children.createCursor();
                while (!cursor.afterLast)
                {
                    if (cursor.current == target)
                    {
                        newLocation++;
                        break;
                    }
                    newLocation += calculateLength(cursor.current, parent) + 1;
                    try
                    {
                        cursor.moveNext();
                    }
                    catch (e:ItemPendingError)
                    {
                        break;
                    }
                }
                target = parent;
                parent = getParentItem(parent);
            }
            cursor = treeData.createCursor();
            while (!cursor.afterLast)
            {
                if (cursor.current == target)
                {
                    newLocation++;
                    break;
                }
                newLocation += calculateLength(cursor.current, parent) + 1;
                try
                {
                    cursor.moveNext();
                }
                catch (e:ItemPendingError)
                {
                    break;
                }
            }
            return newLocation;
        }
    }
}
