package com.brzosthu.collection
{
    import flash.events.EventDispatcher;

    import mx.collections.CursorBookmark;
    import mx.collections.ICollectionView;
    import mx.collections.IHierarchicalCollectionViewCursor;
    import mx.collections.IHierarchicalData;
    import mx.core.EventPriority;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;
    import mx.events.FlexEvent;

    public class SnakeChildCollectionViewCursor extends EventDispatcher implements IHierarchicalCollectionViewCursor
    {
        private static const BEFORE_FIRST_INDEX:int = -1;
        private static const AFTER_LAST_INDEX:int = -2;

        public var sourceCollection:SnakeCollectionView;
        public var childCollectionView:SnakeChildCollectionView;
        public var cursor:NeoCollectionViewCursor;

        private var currentIndex:int;
        private var currentValue:Object;
        private var currentDepthValue:int;

        public function SnakeChildCollectionViewCursor(childCollectionView:SnakeChildCollectionView)
        {
            this.childCollectionView = childCollectionView;
            this.sourceCollection = childCollectionView.sourceCollection;

            cursor = childCollectionView.sourceCollection.createCursor() as NeoCollectionViewCursor;

            sourceCollection.addEventListener(CollectionEvent.COLLECTION_CHANGE,
                    collectionChangeHandler, false, EventPriority.CURSOR_MANAGEMENT, true);

            this.seek(CursorBookmark.FIRST);
        }

        public function get bookmark():CursorBookmark
        {
            if (view.length == 0 || beforeFirst)
            {
                return CursorBookmark.FIRST;
            }

            if (afterLast)
            {
                return CursorBookmark.LAST;
            }

            return new CursorBookmark(localToParentIndex(currentIndex).toString());
        }

        public function get current():Object
        {
            return currentValue;
        }

        public function get currentDepth():int
        {
            return currentDepthValue;
        }

        public function get afterLast():Boolean
        {
            return currentIndex == AFTER_LAST_INDEX || view.length == 0;
        }

        public function get beforeFirst():Boolean
        {
            return currentIndex == BEFORE_FIRST_INDEX || view.length == 0;
        }

        public function get view():ICollectionView
        {
            return childCollectionView;
        }

        public function moveNext():Boolean
        {
            if (afterLast)
            {
                return false;
            }

            var tempIndex:int = beforeFirst ? 0 : currentIndex + 1;
            if (tempIndex >= view.length)
            {
                tempIndex = AFTER_LAST_INDEX;
                setCurrent(null);
            }
            else
            {
                cursor.seek(CursorBookmark.FIRST, localToParentIndex(tempIndex));
                setCurrent(cursor.current, true, cursor.currentDepth);
            }
            currentIndex = tempIndex;
            return !afterLast;
        }

        public function movePrevious():Boolean
        {
            if (beforeFirst)
            {
                return false;
            }

            var tempIndex:int = afterLast ? view.length - 1 : currentIndex - 1;
            if (tempIndex == -1)
            {
                tempIndex = BEFORE_FIRST_INDEX;
                setCurrent(null);
            }
            else
            {
                cursor.seek(CursorBookmark.FIRST, localToParentIndex(tempIndex));
                setCurrent(cursor.current, true, cursor.currentDepth);
            }
            currentIndex = tempIndex;
            return !beforeFirst;
        }

        public function seek(bookmark:CursorBookmark, offset:int = 0, prefetch:int = 0):void
        {
            if (view.length == 0)
            {
                currentIndex = AFTER_LAST_INDEX;
                setCurrent(null, false);
                return;
            }

            var newIndex:int = currentIndex;
            if (bookmark == CursorBookmark.FIRST)
            {
                newIndex = 0;
            }
            else if (bookmark == CursorBookmark.LAST)
            {
                newIndex = view.length - 1;
            }
            else if (bookmark != CursorBookmark.CURRENT)
            {
                /*var message:String;
                 try
                 {
                 newIndex = ListCollectionView(view).getBookmarkIndex(bookmark);
                 if (newIndex < 0)
                 {
                 setCurrent(null);

                 message = resourceManager.getString(
                 "collections", "bookmarkInvalid");
                 throw new CursorError(message);
                 }
                 }
                 catch (bmError:CollectionViewError)
                 {
                 message = resourceManager.getString(
                 "collections", "bookmarkInvalid");
                 throw new CursorError(message);
                 }*/
                newIndex = int(bookmark.value);
            }

            newIndex += offset;

            var newCurrent:Object = null;
            var newDepth:int = 1;
            if (newIndex >= view.length)
            {
                currentIndex = AFTER_LAST_INDEX;
            }
            else if (newIndex < 0)
            {
                currentIndex = BEFORE_FIRST_INDEX;
            }
            else
            {
                cursor.seek(CursorBookmark.FIRST, localToParentIndex(newIndex));
                newCurrent = cursor.current;
                newDepth = cursor.currentDepth;

                currentIndex = newIndex;
            }
            setCurrent(newCurrent, true, newDepth);
        }


        public function insert(item:Object):void
        {

        }

        public function remove():Object
        {
            return null;
        }

        public function findAny(values:Object):Boolean
        {
            seek(CursorBookmark.FIRST);
            var hierarchicalData:IHierarchicalData = childCollectionView.source;

            var done:Boolean = false;
            while (!done)
            {
                var o:Object = hierarchicalData.getData(current);

                var matches:Boolean = true;
                for (var p:String in values)
                {
                    if (o[p] != values[p])
                    {
                        matches = false;
                        break;
                    }
                }

                if (matches)
                    return true;

                done = !moveNext();
            }

            return false;
        }

        public function findFirst(values:Object):Boolean
        {
            return findAny(values);
        }

        public function findLast(values:Object):Boolean
        {
            seek(CursorBookmark.LAST);

            var done:Boolean = false;
            while (!done)
            {
                var o:Object = current;

                var matches:Boolean = true;
                for (var p:String in values)
                {
                    if (o[p] != values[p])
                    {
                        matches = false;
                        break;
                    }
                }

                if (matches)
                    return true;

                done = !movePrevious();
            }

            return false;
        }

        private function setCurrent(value:Object, dispatch:Boolean = true, depth:int = 1):void
        {
            currentValue = value;
            currentDepthValue = depth;

            if (dispatch)
                dispatchEvent(new FlexEvent(FlexEvent.CURSOR_UPDATE));
        }

        private function localToParentIndex(index:int):int
        {
            return childCollectionView.left + index;
        }

        private function collectionChangeHandler(event:CollectionEvent):void
        {
            if (event.kind == CollectionEventKind.UPDATE)
            {
                return;
            }

            if (event.kind == SnakeCollectionEventKind.NODE_OPEN ||
                    event.kind == SnakeCollectionEventKind.NODE_CLOSE)
            {
                var refreshEvent:CollectionEvent = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
                refreshEvent.kind = CollectionEventKind.REFRESH;
                cursor.collectionChangeHandler(refreshEvent);
            }

            seek(CursorBookmark.FIRST, currentIndex);
        }
    }
}
