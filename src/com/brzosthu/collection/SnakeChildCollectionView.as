package com.brzosthu.collection
{
    import com.frishy.utils.LayoutManagerClientHelper;

    import flash.events.Event;
    import flash.events.EventDispatcher;

    import mx.collections.ICollectionView;
    import mx.collections.IHierarchicalCollectionView;
    import mx.collections.IHierarchicalData;
    import mx.collections.ISort;
    import mx.collections.IViewCursor;
    import mx.collections.errors.ItemPendingError;
    import mx.core.EventPriority;
    import mx.core.mx_internal;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;

    use namespace mx_internal;

    public class SnakeChildCollectionView extends EventDispatcher implements IHierarchicalCollectionView
    {

        private var layoutManagerClientHelper:LayoutManagerClientHelper =
                new LayoutManagerClientHelper(commitProperties);

        private var resetRequired:Boolean;
        private var refreshRequired:Boolean;

        public function SnakeChildCollectionView()
        {
            super();
        }

        private var _offset:int;

        [Bindable(event="offsetChanged")]
        public function get offset():int
        {
            return _offset;
        }

        public function set offset(value:int):void
        {
            if (_offset == value) return;
            _offset = value;

            resetRequired = true;
            layoutManagerClientHelper.invalidateProperties();

            dispatchEvent(new Event("offsetChanged"));
        }

        private var _rowCount:int;

        [Bindable(event="rowCountChanged")]
        public function get rowCount():int
        {
            return _rowCount;
        }

        public function set rowCount(value:int):void
        {
            if (_rowCount == value) return;
            _rowCount = value;

            resetRequired = true;
            layoutManagerClientHelper.invalidateProperties();

            dispatchEvent(new Event("rowCountChanged"));
        }

        private var _sourceCollection:SnakeCollectionView;

        [Bindable(event="sourceCollectionChanged")]
        public function get sourceCollection():SnakeCollectionView
        {
            return _sourceCollection;
        }

        public function set sourceCollection(value:SnakeCollectionView):void
        {
            if (_sourceCollection == value) return;

            if (_sourceCollection)
            {
                _sourceCollection.removeEventListener(CollectionEvent.COLLECTION_CHANGE,
                        sourceCollection_collectionChangeHandler);
            }

            _sourceCollection = value;

            if (_sourceCollection)
            {
                _sourceCollection.addEventListener(CollectionEvent.COLLECTION_CHANGE,
                        sourceCollection_collectionChangeHandler, false, EventPriority.CURSOR_MANAGEMENT, true);
            }

            resetRequired = true;
            layoutManagerClientHelper.invalidateProperties();

            dispatchEvent(new Event("sourceCollectionChanged"));
        }

        [Bindable(event="collectionChange")]
        public function get left():int
        {
            return _offset;
        }

        [Bindable(event="collectionChange")]
        public function get right():int
        {
            return _offset + length;
        }

        [Bindable(event="collectionChange")]
        public function get length():int
        {
            if (_sourceCollection)
            {
                return Math.max(0, Math.min(_sourceCollection.length - _offset, _rowCount));
            }
            return 0;
        }

        public function get openNodes():Object
        {
            if (_sourceCollection)
            {
                return _sourceCollection.openNodes;
            }
            return null;
        }

        public function set openNodes(value:Object):void
        {

        }

        public function get hasRoot():Boolean
        {
            if (_sourceCollection)
            {
                return _sourceCollection.hasRoot;
            }
            return false;
        }

        public function get showRoot():Boolean
        {
            if (_sourceCollection)
            {
                return _sourceCollection.showRoot;
            }
            return false;
        }

        public function set showRoot(value:Boolean):void
        {

        }

        public function get source():IHierarchicalData
        {
            if (_sourceCollection)
            {
                return _sourceCollection.source;
            }
            return null;
        }

        public function set source(value:IHierarchicalData):void
        {
        }

        public function openNode(node:Object):void
        {
            if (_sourceCollection)
            {
                _sourceCollection.openNode(node);
            }
        }

        public function closeNode(node:Object):void
        {
            if (_sourceCollection)
            {
                _sourceCollection.closeNode(node);
            }
        }

        public function getChildren(node:Object):ICollectionView
        {
            if (_sourceCollection)
            {
                return _sourceCollection.getChildren(node);
            }
            return null;
        }

        public function addChild(parent:Object, newChild:Object):Boolean
        {
            return false;
        }

        public function removeChild(parent:Object, child:Object):Boolean
        {
            return false;
        }

        public function addChildAt(parent:Object, newChild:Object, index:int):Boolean
        {
            return false;
        }

        public function removeChildAt(parent:Object, index:int):Boolean
        {
            return false;
        }

        public function getNodeDepth(node:Object):int
        {
            if (_sourceCollection)
            {
                return _sourceCollection.getNodeDepth(node);
            }
            return 1;
        }

        public function getParentItem(node:Object):*
        {
            if (_sourceCollection)
            {
                return _sourceCollection.getParentItem(node);
            }
            return null;
        }

        public function get filterFunction():Function
        {
            if (_sourceCollection)
            {
                return _sourceCollection.filterFunction;
            }
            return null;
        }

        public function set filterFunction(value:Function):void
        {

        }

        public function get sort():ISort
        {
            if (_sourceCollection)
            {
                return _sourceCollection.sort;
            }
            return null;
        }

        public function set sort(value:ISort):void
        {
            if (_sourceCollection)
            {
                _sourceCollection.sort = value;
            }
        }

        public function createCursor():IViewCursor
        {
            return new SnakeChildCollectionViewCursor(this);
        }

        public function contains(item:Object):Boolean
        {
            var cursor:IViewCursor = createCursor();
            while (!cursor.afterLast)
            {
                if (cursor.current == item)
                    return true;

                try
                {
                    cursor.moveNext();
                }
                catch (e:ItemPendingError)
                {
                    // item is pending.
                    // we are not sure if the item is present or not,
                    // so return false
                    return false;
                }
            }
            return false;
        }

        public function disableAutoUpdate():void
        {
            if (_sourceCollection)
            {
                _sourceCollection.disableAutoUpdate();
            }
        }

        public function enableAutoUpdate():void
        {
            if (_sourceCollection)
            {
                _sourceCollection.enableAutoUpdate();
            }
        }

        public function itemUpdated(item:Object, property:Object = null,
                                    oldValue:Object = null, newValue:Object = null):void
        {
            if (_sourceCollection)
            {
                _sourceCollection.itemUpdated(item, property, oldValue, newValue);
            }
        }

        public function refresh():Boolean
        {
            if (_sourceCollection)
            {
                return _sourceCollection.refresh();
            }
            return false;
        }

        private function commitProperties():void
        {
            var event:CollectionEvent;

            if (resetRequired || refreshRequired)
            {
                refreshRequired = false;
                resetRequired = false;

                event = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
                event.kind = CollectionEventKind.REFRESH;
                trace("dispatching child refresh");
                dispatchEvent(event);
            }
        }

        mx_internal function validateNow():void
        {
            layoutManagerClientHelper.validateNow();
        }

        private function isLocationBefore(location:int):Boolean
        {
            return location < left;
        }

        private function isLocationIn(location:int):Boolean
        {
            return location >= left && location < right;
        }

        private function shouldRefresh(event:CollectionEvent):Boolean
        {
            switch (event.kind)
            {
                case CollectionEventKind.RESET:
                case CollectionEventKind.REFRESH:
                    return true;

                case CollectionEventKind.REMOVE:
                case CollectionEventKind.ADD:
                case SnakeCollectionEventKind.NODE_CLOSE:
                case SnakeCollectionEventKind.NODE_OPEN:
                    return isLocationBefore(event.location);

                case CollectionEventKind.MOVE:
                case CollectionEventKind.REPLACE:
                    return isLocationIn(event.location) || isLocationIn(event.oldLocation);
            }

            return false;
        }

        private function sourceCollection_collectionChangeHandler(event:CollectionEvent):void
        {
            if (shouldRefresh(event))
            {
                refreshRequired = true;
                layoutManagerClientHelper.invalidateProperties();
            }
        }
    }
}
