package com.brzosthu.collection
{
    import flash.events.Event;

    import mx.collections.ArrayCollection;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;

    public class SnakeArrayCollection extends ArrayCollection
    {
        public function SnakeArrayCollection()
        {
            super();
            addEventListener(CollectionEvent.COLLECTION_CHANGE,
                    collectionChangeHandler);
        }

        private var _sourceCollection:SnakeCollectionView;

        [Bindable(event="sourceCollectionChanged")]
        public function get sourceCollection():SnakeCollectionView
        {
            return _sourceCollection;
        }

        public function set sourceCollection(value:SnakeCollectionView):void
        {
            if (_sourceCollection == value) return;

            if (_sourceCollection)
            {
                _sourceCollection.removeEventListener(CollectionEvent.COLLECTION_CHANGE,
                        sourceCollection_collectionChangeHandler);
            }

            _sourceCollection = value;

            if (_sourceCollection)
            {
                _sourceCollection.addEventListener(CollectionEvent.COLLECTION_CHANGE,
                        sourceCollection_collectionChangeHandler, false, 0, true);
            }

            handleResetOrRefresh();

            dispatchEvent(new Event("sourceCollectionChanged"));
        }

        private var _rowCount:int = -1;

        [Bindable(event="rowCountChanged")]
        public function get rowCount():int
        {
            return _rowCount;
        }

        public function set rowCount(value:int):void
        {
            if (_rowCount == value) return;
            _rowCount = value;
            handleResetOrRefresh();
            dispatchEvent(new Event("rowCountChanged"));
        }

        private function handleResetOrRefresh():void
        {
            if (!_sourceCollection) return;

            var newLength:int = calculateLength();
            var currentLength:int = length;
            if (newLength == currentLength) return;

            for (var i:int = 0; i < currentLength; i++)
            {
                var childCollection:SnakeChildCollectionView = getItemAt(i) as SnakeChildCollectionView;
                if (childCollection)
                {
                    childCollection.rowCount = _rowCount;
                    childCollection.offset = i * _rowCount;
                    childCollection.sourceCollection = _sourceCollection;
                }
            }

            if (newLength > currentLength)
            {
                handleAdd();
                return;
            }

            handleRemove();
        }

        private function handleAdd():void
        {
            if (!_sourceCollection) return;

            var newLength:int = calculateLength();
            var currentLength:int = length;
            if (newLength <= currentLength) return;

            for (var i:uint = currentLength; i < newLength; i++)
            {
                var childCollection:SnakeChildCollectionView = new SnakeChildCollectionView();
                if (!childCollection)
                {
                    childCollection = new SnakeChildCollectionView();
                }

                childCollection.rowCount = _rowCount;
                childCollection.offset = i * _rowCount;
                childCollection.sourceCollection = _sourceCollection;
                addItem(childCollection);
            }
        }

        private function handleRemove():void
        {
            if (!_sourceCollection) return;

            var newLength:int = calculateLength();
            var currentLength:int = length;
            if (newLength >= currentLength) return;

            for (var i:uint = currentLength; i > newLength; i--)
            {
                var childCollection:SnakeChildCollectionView = removeItemAt(i - 1) as SnakeChildCollectionView;
                childCollection.source = null;
            }
        }

        private function calculateLength():int
        {
            if (!_sourceCollection)
            {
                return 0;
            }
            return Math.max(2, Math.ceil(_sourceCollection.length / _rowCount));
        }

        private function sourceCollection_collectionChangeHandler(event:CollectionEvent):void
        {
            switch (event.kind)
            {
                case CollectionEventKind.ADD:
                case SnakeCollectionEventKind.NODE_OPEN:
                    handleAdd();
                    return;

                case CollectionEventKind.REMOVE:
                case SnakeCollectionEventKind.NODE_CLOSE:
                    handleRemove();
                    return;

                case CollectionEventKind.REFRESH:
                case CollectionEventKind.RESET:
                    handleResetOrRefresh();
                    return;

                case CollectionEventKind.REPLACE:
                case CollectionEventKind.MOVE:
                case CollectionEventKind.UPDATE:
                //do nothing handled by child collections
            }
        }

        private function collectionChangeHandler(event:CollectionEvent):void
        {
            if (event.kind != CollectionEventKind.UPDATE)
            {
                trace(event.kind);
            }
        }
    }
}
