#!/usr/bin/env bash

for KILLPID in `ps ax | grep 'adl' | awk ' { print $1;}'`; do
  kill -9 $KILLPID;
done

exit 1