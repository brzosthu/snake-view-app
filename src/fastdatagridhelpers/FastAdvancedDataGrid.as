package fastdatagridhelpers
{
    import flash.events.Event;

    import mx.collections.IHierarchicalCollectionViewCursor;

    import mx.controls.AdvancedDataGrid;
    import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
    import mx.controls.advancedDataGridClasses.AdvancedDataGridColumnGroup;
    import mx.controls.advancedDataGridClasses.AdvancedDataGridListData;
    import mx.controls.listClasses.BaseListData;
    import mx.core.mx_internal;
    import mx.events.CollectionEvent;
    import mx.events.CollectionEventKind;

    use namespace mx_internal;

    public class FastAdvancedDataGrid extends AdvancedDataGrid
    {
        public function FastAdvancedDataGrid()
        {
            super();
        }

        override protected function makeListData(data:Object, uid:String,
                                                 rowNum:int, columnNum:int, column:AdvancedDataGridColumn):BaseListData
        {
            var advancedDataGridListData:AdvancedDataGridListData = super.makeListData(data, uid, rowNum, columnNum, column) as AdvancedDataGridListData;

            if (iterator && iterator is IHierarchicalCollectionViewCursor && columnNum == treeColumnIndex
                    && !(data is AdvancedDataGridColumn))
                initListData(data, advancedDataGridListData);


            return advancedDataGridListData;
        }

        override protected function collectionChangeHandler(event:Event):void
        {
            var ce:CollectionEvent = CollectionEvent(event);
            if (ce.kind != CollectionEventKind.UPDATE)
            {
                super.collectionChangeHandler(event);
            }
        }
    }
}