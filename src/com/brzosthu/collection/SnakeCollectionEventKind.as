package com.brzosthu.collection
{
    public class SnakeCollectionEventKind
    {
        public static const NODE_OPEN:String = "nodeOpen";
        public static const NODE_CLOSE:String = "nodeClose";
    }
}
